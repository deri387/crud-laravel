<?php

use App\Http\Controllers\MahasiswaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MahasiswaController::class, 'index'])->name('mahasiswa.index');
Route::get('/add', [MahasiswaController::class, 'create'])->name('mahasiswa.add');
Route::post('/store', [MahasiswaController::class, 'store'])->name('mahasiswa.store');
Route::get('/edit/{id}', [MahasiswaController::class, 'edit'])->name('mahasiswa.edit');
Route::post('/update', [MahasiswaController::class, 'update'])->name('mahasiswa.update');
Route::post('/delete', [MahasiswaController::class, 'delete'])->name('mahasiswa.delete');
