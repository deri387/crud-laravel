@extends("layouts.base")
@section("content")
<div class="container p-5">
    <div class="d-flex items-center w-full">
        <h3>Latihan CRUD</h3>
        <div class="ms-auto">
            <a href="/add" class="btn btn-sm btn-success">Create data</a>
        </div>
    </div>
    <div class="mt-5">
        <table class="table table-responsive table-striped table-bordered">
            <thead>
                <th>No</th>
                <th>Name</th>
                <th>Class</th>
                <th>#</th>
            </thead>
            <tbody>
                @forelse ($dataMahasiswa as $key => $item)
                <tr>
                    <td>
                        <span
                            class="text-dark text-hover-primary d-block mb-1 fs-6">{{($dataMahasiswa->currentPage() - 1) * $dataMahasiswa->perPage() + $loop->iteration}}</span>
                    </td>
                    <td>
                        <span class="text-dark d-block mb-1 fs-6">{{ $item->name }}</span>
                    </td>
                    <td>
                        <span class="text-dark d-block mb-1 fs-6">{{ $item->course }}</span>
                    </td>
                    <td class="text-center">
                        <a href="/edit/{{ $item->id}}" class="btn btn-sm btn-primary mb-3">Edit</a>
                        <form method="POST" action="/delete">
                            @csrf
                            <input type="hidden" name="id" value="{{ $item->id }}" />
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">
                        <span class="text-dark fw-bolder d-block mb-1 fs-6 text-center">Empty
                            data</span>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <div class="card-footer border-0 pt-0">
            <div class="d-flex flex-wrap justify-content-end">
                {{ $dataMahasiswa->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
