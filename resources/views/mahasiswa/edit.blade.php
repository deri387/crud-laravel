@extends("layouts.base")
@section("content")
<div class="container p-5">
    <div class="d-flex items-center w-full">
        <h3>Edit Data</h3>
    </div>
    <div class="mt-5">
        <form action="/update" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $dataMahasiswa->id }}">
            <div class="form-group">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" placeholder="Name" id="name" name="name"
                    value="{{ $dataMahasiswa->name }}">
            </div>
            <div class="form-group mt-3">
                <label for="course" class="form-label">Course</label>
                <input type="text" class="form-control" placeholder="Class" id="course" name="course"
                    value="{{ $dataMahasiswa->course }}">
            </div>
            <div class="mt-3">
                <button type="submit" class="btn btn-sm btn-success">Save</button>
                <a href="{{ route('mahasiswa.index') }}" class="btn btn-sm btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
@endsection
