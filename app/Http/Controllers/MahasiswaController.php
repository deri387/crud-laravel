<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MahasiswaController extends Controller
{
    /**
     * Function ini berguna sebagai index halaman yang akan di render
     * Memanggil data dari database dengan ORM pada model Mahasiswa
     * Limit 10 Data dan order berdasarkan data terbaru
     */
    public function index() {
        try {
            $dataMahasiswa = Mahasiswa::orderBy("created_at","desc")->paginate(10);
            return view("mahasiswa.index", compact("dataMahasiswa"));
        } catch (\Throwable $th) {
            return redirect()->back()->with(['error' => $th->getMessage()]);
        }
    }

    /**
     * Function ini berguna sebagai rendering halaman tambah data mahasiswa
     */
    public function create() {
        return view("mahasiswa.add");
    }

    /**
     * Function ini berguna sebagai passing data dari form halaman tambah disimpan ke database
     * Terdapat validasi form yaitu mengecek data yang tidak diisi
     */
    public function store(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'course' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                            ->with(['error' => $validator->errors()->first()])
                            ->withInput();
            }

            $dataMahasiswa = new Mahasiswa();
            $dataMahasiswa->name = $request->name;
            $dataMahasiswa->course = $request->course;
            $dataMahasiswa->save();

            return redirect()->back()->with(['message' => 'Succesfully add new data.']);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['error' => $th->getMessage()]);
        }
    }

    /**
     * Function ini berguna sebagai rendering halaman edit data mahasiswa
     * Retrieve data mahasiswa berdasarkan id yang dikirim
     */
    public function edit($id) {
        try {
            $dataMahasiswa = Mahasiswa::where("id", $id)->first();
            if (!$dataMahasiswa) {
                abort(400);
            } 

            return view("mahasiswa.edit", compact("dataMahasiswa"));
        } catch (\Throwable $th) {
            return redirect()->back()->with(['error' => $th->getMessage()]);
        }
    }

    /**
     * Function ini berguna sebagai passing data dari form halaman edit diupdate ke database
     * Terdapat validasi form yaitu mengecek data yang tidak diisi
     */
    public function update(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'course' => 'required',
                'id'    => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                            ->with(['error' => $validator->errors()->first()])
                            ->withInput();
            }

            $dataMahasiswa = Mahasiswa::where("id", $request->id)->first();
            if (!$dataMahasiswa) {
                abort(400);
            }

            $dataMahasiswa->name = $request->name;
            $dataMahasiswa->course = $request->course;
            $dataMahasiswa->save();

            return redirect()->back()->with(['message' => 'Succesfully update.']);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['error' => $th->getMessage()]);
        }
    }

    /**
     * Function ini berguna sebagai menghapus data dari database berdasarkan id yang dipilih
     * Terdapat validasi form yaitu mengecek data yang tidak diisi
     */
    public function delete(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'id'    => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                            ->with(['error' => $validator->errors()->first()])
                            ->withInput();
            }

            $dataMahasiswa = Mahasiswa::where("id", $request->id)->first();
            if (!$dataMahasiswa) {
                abort(400);
            }

            $dataMahasiswa->delete();

            return redirect()->back()->with(['message' => 'Succesfully delete.']);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['error' => $th->getMessage()]);
        }
    }
}
